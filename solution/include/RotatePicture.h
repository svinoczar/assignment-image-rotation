#ifndef ROTATE_PICTURE_H
#define ROTATE_PICTURE_H

#include "ImagePixelStructure.h"

struct image rotate(struct image source);

#endif
